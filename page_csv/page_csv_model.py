#!/usr/bin/env python
# Copyright (c) 2019,2018 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause

# python ./page_csv_model.py pages3.csv

#from __future__ import with_statement

#from collections import defaultdict,namedtuple,OrderedDict
from collections import OrderedDict
import csv
from datetime import datetime as dt
import fileinput
from os import path,sep
import re
from sys import argv
try:
	#import StringIO
	import io
except ImportError:
	print('Unable to find StringIO - are you using suitable Py version?')


from string import ascii_letters,digits,maketrans,printable
#SET_LOWERUPPER_AND_DIGITS=set(''.join([ascii_lowercase,ascii_uppercase,digits]))
SET_LOWERUPPER_AND_DIGITS=set(ascii_letters).union(digits)
SET_PRINTABLE = set(printable)
#SET_PRINTABLE_NOT_PERIOD = set(printable).difference(set(chr(46)))
DOUBLEQUOTES_INSTEAD = maketrans('[]', '""')
RE_COMMAS_END=re.compile(',+\n')
RE_NUMSUFFIX=re.compile(r'\d+$')


MODELS_HEAD="""
import datetime

from django.db import models
from django.utils import timezone

"""

MODEL_CTEMPLATE="""
class {0}(models.Model):
    mfield = models.{1}
    
    def __str__(self):
        return self.mfield
"""


def dt_iso(datetime_given=None):
	if datetime_given is None:
		str_iso = datetime.datetime.isoformat(datetime.datetime.now())
	else:
		str_iso = datetime.datetime.isoformat(datetime_given)
	return str_iso
	
	
def case_camel(str_given):
	str_array = str_given.split('_')
	str_camel = ''.join(item.title() for item in str_array)
	return str_camel


def models_dot(field_type,field_mod,validators=None,choices=None,default=None,verbose_name=None):
	mlen = 0
	dot_suffix = 'CharField'
	if 'date' == field_type:
		dot_suffix = 'DateField'
	elif 'int' == field_type:
		dot_suffix = 'IntegerField'
	elif 'float2' == field_type:
		dot_suffix = 'DecimalField'
		#mlen = 2
		dplaces = 2
	elif field_type.startswith('chr'):
		pos = RE_NUMSUFFIX.search(field_type)
		if pos > 0:
			try:
				mlen = int(field_type[pos:])
			except:
				pass
	else:
		pass
		
	if verbose_name is None:
		args="verbose_name=''"
	else:
		args="verbose_name='{}'".format(verbose_name)
	# Above we have deliberately set verbose_name in all cases so comma append more predictable below.
	
	if 'CharField' == dot_suffix:
		if mlen > 0:
			args = ",max_length={0}".format(mlen)
	elif 'Decimal' == dot_suffix:
		if dplaces > 0:
			args = ",decimal_places={0}".format(dplaces)
	elif validators is not None:
		args+=",validators={0}".format(validators)
	elif choices is not None:
		#if hasattr(choices, '__iter__')
		args+=",choices={0}".format(choices)		
	elif default is not None:
		args+=",default={0}".format(default)		
	else:
		pass
		
	dot_suffix = "{0}({1})".format(dot_suffix,args)
	
	return dot_suffix


def models_dot_wrapper(field_type,field_mod,validators,choices,default,verbose_name):
	""" Wrapper around models_dot that makes empty strings None and other preprocessing. """
	if validators is None or len(validators) < 1:
		validators = None
	if choices is None or len(choices) < 1:
		choices = None
	if default is None or len(default) < 1:
		default = None
	return models_dot(field_type,field_mod,validators,choices,default,verbose_name)


def csv_dict(fileinput_given,verbosity=0):
	
	fdict = OrderedDict()
	# Above we define an empty [Ordered] dict to hold the field specifications
	try:
		lines = fileinput.input(fileinput_given)
	except:
		lines = []
		raise
		
	stored_line=''
	stored_line_old=''
	idx_pages=1			# start at 1 so 'pagination1' is marker between pages 1 and 2
	for line in lines:
		try:
			reader = csv.reader(io.StringIO(unicode(line)))
			for row in reader:
				fname = row[0]
				if len(fname) < 2:
					if '' == fname.strip():
						fname = "pagination{}".format(idx_pages)
						idx_pages+=1
				elif set(fname).issubset(SET_PRINTABLE):
					# Everything ok about the field name (fname)
					pass
				else:
					raise ValueError
				spec_list = [row[1],row[2],row[3],row[6],row[7],row[8:]]
				fdict[fname]=spec_list
				# [0] is name (fname)
				# [1] is field type
				# [2] is field modifier such as 'mand' or 'autoid'
				# [3] is description
				# [6] is valid lower
				# [7] is valid upper
				# [8:]rest of the line
				if verbosity > 0:
					print(fname,case_camel(fname),row[1])
		except TypeError:
			print('row parse issue TypeError')
			raise
		except:
			raise
		stored_line_old=stored_line
		stored_line=line
		if RE_COMMAS_END.match(stored_line) and RE_COMMAS_END.match(stored_line_old):
			print('Terminating parsing because encountered two blank rows.')
			break
		
	return fdict


def models_gen(field_dict={},filepath='/tmp/models.py'):
	""" Generate a models.py from the supplied dict (OrderedDict)
	Return a two tuple:
		Count of how many items successfully modelled
		dict of feedback
	"""
	mcount = 0
	feedback_dict=OrderedDict()
	try:
		with open(filepath, 'w') as outf:
			outf.write(MODELS_HEAD)
			for idx,fname in enumerate(field_dict):
				if fname.startswith('pagination'):
					continue
				spec_list = field_dict[fname]
				print(fname,spec_list)
				try:
					ftype = spec_list[0]	# original row[1]
					fmod = spec_list[1]		# original row[2]
					fdesc = spec_list[2]	# original row[3]
					flower = spec_list[3]	# original row[6]
					fupper = spec_list[4]	# original row[7]
					frest = spec_list[5]	# original row[8:]
					idx+=1
				except:
					feedback_dict[idx]="Error initialising model field=".format(idx)
					#break
				dot_suffix = models_dot_wrapper(ftype,fmod,validators=None,choices=None,default=None,verbose_name=fdesc)
				outf.write(MODEL_CTEMPLATE.format(case_camel(fname),dot_suffix))
				outf.write('\n')
			outf.write("### generated at {}".format(dt_iso()))
			outf.flush()
	except:
		feedback_dict['error'] = "Error opening file at {0}".format(filepath)
	return (mcount,feedback_dict)
	

if __name__ == "__main__":

	exit_rc = 0

	DEBUG = False

	"""if len(argv) > 1:
		pattern_given = argv[1]
	"""
	
	fileinput_given = argv[1:]
	fdict = csv_dict(fileinput_given,1)
	mcount, feedback_dict = models_gen(fdict)
	if mcount and len(feedback_dict) > 0:
		print("Fields processed count is {}".format(mcount))
		for f in feedback_dict:
			print(feedback[f])
	exit(exit_rc)



